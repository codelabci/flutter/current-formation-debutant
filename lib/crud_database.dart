import 'package:flutter/material.dart';
import 'package:flutterformation/edit_user.dart';
import 'package:flutterformation/main.dart';

class CrudDatabase extends StatefulWidget {
  CrudDatabase({Key? key}) : super(key: key);

  @override
  State<CrudDatabase> createState() => _CrudDatabaseState();
}

class _CrudDatabaseState extends State<CrudDatabase> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Base de données en locale"),
            automaticallyImplyLeading: false, // remove callback button
            bottom: const TabBar(
              tabs: <Widget>[
                Tab(
                  icon: Icon(Icons.edit),
                  text: "Editer user",
                ),
                Tab(
                  icon: Icon(Icons.list),
                  text: "Users",
                ),
              ],
            ),
          ),
          body: TabBarView(
            // controller: tabController,
            children: <Widget>[
              EditUser(
                isDatabaseData: true,
              ),
              Home(
                isDatabaseData: true,
              ),
            ],
          ),
        ));
  }
}
