import 'package:flutter/material.dart';
import 'package:flutterformation/constant.dart';
import 'package:flutterformation/crud_database.dart';
import 'package:flutterformation/edit_user.dart';
import 'package:flutterformation/repository.dart';
import 'package:flutterformation/telephone_app.dart';
import 'package:flutterformation/user.dart';
import 'package:flutterformation/utilities.dart';
import 'package:lottie/lottie.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Codelab Demo ',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const LandingPage(),
      // routes: {
      //   // When navigating to the "/" route, build the FirstScreen widget.
      //   '/': (context) => const LandingPage(),
      //   // When navigating to the "/second" route, build the SecondScreen widget.
      //   '/editUser': (context) => const EditUser(),
      // },
    );
  }
}

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Scaffold(
            backgroundColor: Colors.grey.shade300,
            // appBar: AppBar(
            //   elevation: 0,
            //   title: Text("Landing"),
            // ),
            body: Center(
              child: Column(
                // mainAxisAlignment: Main,
                mainAxisAlignment: MainAxisAlignment.center,
                // crossAxisAlignment: CrossAxisAlignment.,

                children: [
                  Text("Bonjour"),
                  SizedBox(
                      height: MediaQuery.of(context).size.height * 0.3,
                      // width: MediaQuery.of(context).size.width,
                      child: Lottie.asset("assets/108333-coding.json")),
                  Text("Bienvenue à notre formation"),
                ],
              ),
            ))
        : Home();
  }

  initData() {
    Future.delayed(const Duration(seconds: 6), () {
      setState(() {
        isLoading = false;
      });
    });
  }
}

class Home extends StatefulWidget {
  Home({Key? key, this.isDatabaseData = false}) : super(key: key);
  bool isDatabaseData;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<User> currentDatasUser = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  initData() async {
    if (widget.isDatabaseData) {
      var list = await SQLHelper.getUsers();

      print(list);

      for (var element in list) {
        currentDatasUser.add(User.fromMap(element));
      }
      setState(() {});
    } else {
      currentDatasUser = datasUser;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        // backgroundColor: Colors.grey.shade100,
        // elevation: 0,
        semanticLabel: "label",
        child: ListView(
          children: [
            DrawerHeader(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CircleAvatar(),
                      IconButton(
                          onPressed: () {}, icon: Icon(Icons.nightlight)),
                    ],
                  ),
                  ListTile(
                    title: Text("Kamagate"),
                    subtitle: Text("0778908790"),
                    trailing: Icon(Icons.home),
                    onTap: () {},
                  )
                ],
              ),
              decoration: BoxDecoration(color: Colors.grey.shade400),
            ),
            ListTile(
              title: Text("CRUD Database"),
              leading: Icon(Icons.phone),
              onTap: () {
                Navigator.pop(context);
                Utilities.push(context: context, widget: CrudDatabase());
              },
            ),
            ListTile(
              title: Text("Telephone App"),
              leading: Icon(Icons.phone),
              onTap: () {
                Navigator.pop(context);
                Utilities.push(context: context, widget: TelephoneApp());
              },
            ),
            ListTile(
              title: Text("Message App"),
              leading: Icon(Icons.message),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text("Use API"),
              leading: Icon(Icons.api),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text("Connexion"),
              leading: Icon(Icons.sign_language),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            Divider(),
            ListTile(
              title: Text("Exit"),
              leading: Icon(Icons.exit_to_app),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      appBar: widget.isDatabaseData
          ? null
          : AppBar(
              title: Text('Accueil'),
              actions: [
                IconButton(
                    onPressed: () {
                      print("cliqué");
                    },
                    icon: Icon(Icons.search)),
                IconButton(
                    onPressed: () {
                      print("cliqué");
                    },
                    icon: Icon(Icons.home)),
              ],
            ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              color: Colors.grey,
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Text("Liste des utilisateurs",
                        style: TextStyle(
                            fontSize: 50, fontWeight: FontWeight.w100)),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      // Navigator.pushNamed(
                      //   context, 'editUser'
                      //   // MaterialPageRoute(builder: (context) => EditUser()),
                      // );

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditUser(
                                  isDatabaseData: widget.isDatabaseData,
                                )),
                      );
                    },
                    child: Text("Ajouter"),
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView.separated(
                reverse: true,
                itemBuilder: (context, index) {
                  var currentUser = currentDatasUser[index];
                  return Card(
                    child: ListTile(
                      title: Text(currentUser.nom + " $index"),
                      subtitle: Text((currentUser.prenoms != null
                              ? currentUser.prenoms!
                              : "") +
                          " $index"),
                      leading: Icon(Icons.home),
                      trailing: IconButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditUser(
                                      currentUser: currentUser,
                                      isDatabaseData: widget.isDatabaseData)),
                            );
                          },
                          icon: Icon(Icons.edit)),
                      onTap: () => deleteUser(user: currentUser),
                    ),
                  );
                },
                itemCount: currentDatasUser.length,
                separatorBuilder: (context, index) {
                  return Divider(
                      // height: 10,
                      // color: Colors.red,
                      // indent: 30,
                      );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  deleteUser({required User user}) {
    print(":::: deleteUser ::::");

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          // icon: Icon(Icons.home),
          // shape: getDefaultShape(),
          title: const Text("Boite de dialogue"),
          content: const Text(
            "Etes vous sûr de supprimer cet user ?",
          ),
          actionsAlignment: MainAxisAlignment.spaceBetween,

          // alignment: Alignment.bottomCenter,
          actions: <Widget>[
            TextButton(
              child: Text("Non"),
              onPressed: () {
                print(":::: Non ::::");

                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text("Oui"),
              onPressed: () async {
                print(":::: Oui ::::");

                if (widget.isDatabaseData) {
                  await SQLHelper.deleteUser(user.id!);
                }
                currentDatasUser.remove(user);

                // datasUser = [...defaultDatasUser];
                setState(() {});
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
