import 'package:flutter/material.dart';
import 'package:flutterformation/constant.dart';

class TelephoneApp extends StatefulWidget {
  TelephoneApp({Key? key}) : super(key: key);

  @override
  State<TelephoneApp> createState() => _TelephoneAppState();
}

class _TelephoneAppState extends State<TelephoneApp> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text("Telephone App"),
          bottom: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.edit),
                // text: "Editer user",
              ),
              Tab(
                icon: Icon(Icons.date_range),
                // text: "Users",
              ),
              Tab(
                icon: Icon(Icons.contact_support),
                // text: "Users",
              ),
            ],
          ),
          actions: [
            IconButton(onPressed: () {}, icon: Icon(Icons.search)),
            PopupMenuButton(
                itemBuilder: (context) => [
                      PopupMenuItem(
                        child: Text("Appel à afficher"),
                        onTap: () {
                          print("Appel à afficher");
                        },
                      ),
                      PopupMenuItem(
                        child: Text("Supprimer"),
                      ),
                      PopupMenuItem(
                        child: Text("Afficher les messages"),
                      ),
                      PopupMenuItem(
                        child: Text("Durée total des appels"),
                      ),
                      PopupMenuItem(
                        child: Text("Parmètres"),
                      ),
                    ])
          ],
        ),
        body: TabBarView(children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: [
                Text(
                  "Appels suggérés",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      Text(" ghkjlm yftguhijk trfyguhijk fytguhijk ghjk"),
                      Text(" ghkjlm yftguhijk trfyguhijk fytguhijk ghjk"),
                      Text(" ghkjlm yftguhijk trfyguhijk fytguhijk ghjk"),
                    ],
                  ),
                ),
                Text(
                  "Favoris",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                GridView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  children: [for (var data in datasContact) Text(data)],
                )
              ],
            ),
          ),
          Center(
            child: Text("2"),
          ),
          Center(
            child: Text("3"),
          ),
        ]),
      ),
    );
  }
}
