import 'package:flutter/foundation.dart';
import 'package:flutterformation/user.dart';
import 'package:flutterformation/utilities.dart';
import 'package:sqflite/sqflite.dart' as sql;

// id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE user(
        id TEXT PRIMARY KEY NOT NULL,
        nom TEXT,
        prenoms TEXT,
        telephone TEXT,
        email TEXT,
        password TEXT
      )
      """);
  }
// id: the id of a user
// created_at: the time that the user was created. It will be automatically handled by SQLite

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'codelabci.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // Create new item (journal)
  static Future<dynamic> createUser(User data) async {
    Utilities.begin("createUser");

    final db = await SQLHelper.db();
    // data.createdAt = DateTime.now().toString();

    final id = await db.insert('user', data.toMap(),
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    print(id);
    return id;
  }

  // Read all user (journals)
  static Future<List<Map<String, dynamic>>> getUsers() async {
    final db = await SQLHelper.db();
    return db.query('user', orderBy: "id");
  }

  // Read a single item by id
  // The app doesn't use this method but I put here in case you want to see it
  static Future<List<Map<String, dynamic>>> getUser(int id) async {
    final db = await SQLHelper.db();
    return db.query('user', where: "id = ?", whereArgs: [id], limit: 1);
  }

  // Update an item by id
  static Future<dynamic> updateUser(User data) async {
    Utilities.begin("updateUser");
    final db = await SQLHelper.db();

    // data.createdAt = DateTime.now().toString();

    final result = await db
        .update('user', data.toMap(), where: "id = ?", whereArgs: [data.id]);
    print(result);
    return result;
  }

  // Delete
  static Future<void> deleteUser(String id) async {
    final db = await SQLHelper.db();
    try {
      await db.delete("user", where: "id = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an user: $err");
    }
  }
}
