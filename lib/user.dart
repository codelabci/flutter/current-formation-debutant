import 'dart:convert';

import 'package:flutter/material.dart';

class User {
  String? id;
  String nom;
  String? prenoms;
  String? email;
  String? telephone;
  String? password;
  User({
    this.id = "ghhjkl",
    required this.nom,
    this.prenoms,
    this.email,
    this.telephone,
    this.password,
  });
  // bool? isOk;

  User copyWith({
    String? id,
    String? nom,
    String? prenoms,
    String? email,
    String? telephone,
    String? password,
  }) {
    return User(
      id: id ?? this.id,
      nom: nom ?? this.nom,
      prenoms: prenoms ?? this.prenoms,
      email: email ?? this.email,
      telephone: telephone ?? this.telephone,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nom': nom,
      'prenoms': prenoms,
      'email': email,
      'telephone': telephone,
      'password': password,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id'],
      nom: map['nom'] ?? '',
      prenoms: map['prenoms'],
      email: map['email'],
      telephone: map['telephone'],
      password: map['password'],
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  @override
  String toString() {
    return 'User(id: $id, nom: $nom, prenoms: $prenoms, email: $email, telephone: $telephone, password: $password)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is User &&
        other.id == id &&
        other.nom == nom &&
        other.prenoms == prenoms &&
        other.email == email &&
        other.telephone == telephone &&
        other.password == password;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        nom.hashCode ^
        prenoms.hashCode ^
        email.hashCode ^
        telephone.hashCode ^
        password.hashCode;
  }
}
