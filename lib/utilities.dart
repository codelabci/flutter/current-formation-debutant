import 'package:flutter/material.dart';
import 'package:flutterformation/main.dart';

class Utilities {
  static begin(element) {
    print("*********************BEGIN $element*********************");
  }

  static log(element) {
    print("*********************LOG $element*********************");
  }

  static end(element) {
    print("*********************END $element*********************");
  }

  static resetAndOpenPage({required dynamic context, dynamic view}) {
    // static resetAndOpenPage( dynamic context, dynamic view) {
    begin("resetAndOpenPage");
    //pushNamedAndRemoveUntil
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => view ?? Home()),
        (Route<dynamic> route) => false

        //ModalRoute.withName('/'),
        );
    end("resetAndOpenPage");
  }

  static push({required BuildContext context, dynamic widget}) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => widget),
    );
  }
}
