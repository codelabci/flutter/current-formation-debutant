import 'package:flutter/material.dart';
import 'package:flutterformation/constant.dart';
import 'package:flutterformation/repository.dart';
import 'package:flutterformation/user.dart';
import 'package:flutterformation/utilities.dart';

class EditUser extends StatefulWidget {
  EditUser({super.key, this.currentUser, this.isDatabaseData = false});

  User? currentUser;

  bool isDatabaseData;

  @override
  State<EditUser> createState() => _EditUserState();
}

class _EditUserState extends State<EditUser> {
  final formKey = GlobalKey<FormState>();

  User data = User(nom: "");

  TextEditingController nomController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nomController.text = widget.currentUser?.nom ?? '';

    // data.isOk = widget.currentUser?.isOk ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.isDatabaseData
          ? null
          : AppBar(
              title: Text("Edit user"),
            ),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                SizedBox(height: 10),
                TextFormField(
                  // autovalidateMode: AutovalidateMode.always,
                  // initialValue: widget.currentUser?.nom,

                  controller: nomController,

                  onSaved: (value) {
                    data.nom = value ?? "";
                    // if (widget.currentUser != null) {
                    //   widget.currentUser!.nom = value ?? "";
                    // }
                  },
                  onChanged: (value) {},
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Champ requis";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    label: Text("Nom *"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                ),
                SizedBox(height: 10),
                TextFormField(
                  initialValue: widget.currentUser?.prenoms,
                  onSaved: (value) {
                    data.prenoms = value;
                  },
                  keyboardType: TextInputType.datetime,
                  decoration: InputDecoration(
                    label: Text("Prenoms"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                ),
                SizedBox(height: 10),
                TextFormField(
                  initialValue: widget.currentUser?.telephone,
                  onSaved: (value) {
                    data.telephone = value;
                  },
                  decoration: InputDecoration(
                    label: Text("Telephone"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                ),
                SizedBox(height: 10),
                TextFormField(
                  initialValue: widget.currentUser?.email,
                  onSaved: (value) {
                    data.email = value;
                  },
                  decoration: InputDecoration(
                    label: Text("Email"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                ),
                SizedBox(height: 10),
                TextFormField(
                  initialValue: widget.currentUser?.password,
                  onSaved: (value) {
                    data.password = value;
                  },
                  decoration: InputDecoration(
                    label: Text("Password"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                ),
                SizedBox(height: 10),
                // Checkbox(
                //   value: data.isOk,
                //   onChanged: (value) {
                //     data.isOk = value;

                //     setState(() {});
                //   },
                // ),
                // SizedBox(height: 10),
                ElevatedButton(
                    onPressed: () async {
                      var formState = formKey.currentState!;

                      if (formState.validate()) {
                        print("before  save:: $data");

                        formState.save();

                        // formState.reset();

                        // traitements
                        if (widget.currentUser == null) {
                          if (widget.isDatabaseData) {
                            await SQLHelper.createUser(data);
                          } else {
                            datasUser.add(data);
                          }
                        } else {
                          if (widget.isDatabaseData) {
                            await SQLHelper.updateUser(data);
                          } else {
                            // copy des champ du data dans le cureentUser
                            int indexCurrentUser =
                                datasUser.indexOf(widget.currentUser!);
                            datasUser[indexCurrentUser] = data;
                          }
                        }

                        /// navigation : suppression de toute la stack de widget et envoi du Home
                        Utilities.resetAndOpenPage(
                          context: context,
                        );
                        print("after save :: $data");
                      } else {}
                    },
                    child: Text(
                        widget.currentUser != null ? "Modifier" : "Ajouter"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
